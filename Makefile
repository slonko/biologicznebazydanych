startDocker:
	docker run -d -p 3306:3306 --name mysql-babd\
		-e MYSQL_ROOT_PASSWORD=p\
		-v `pwd`/mysql-dir:/var/lib/mysql centurylink/mysql
	docker run -d -p 80:80 --name php-babd\
		-v `pwd`/php-app:/app\
		--link mysql-babd:db apache-docker

buildDocker:
	docker build -t apache-docker apache-docker/

stopDocker:
	docker stop mysql-babd php-babd
	docker rm mysql-babd php-babd

loginPhp:
	docker exec -ti php-babd su project -c bash

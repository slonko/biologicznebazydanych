## Instalacja
Zainstaluj Dockera według [instrukcji](https://docs.docker.com/engine/installation/).

Następnie wykonaj następujące kroki:

1. zbuduj Dockera: `make buildDocker`

2. uruchom kontenery: `make startDocker`

3. zinstaluj [Composera](https://getcomposer.org/doc/00-intro.md)

4. zainstaluj potrzebny Yii2 plugin: `composer.phar global require "fxp/composer-asset-plugin:1.2.2"` 

5. w `php-app/` wykonaj `composer.phar install`

6. następnie wgraj w folderze głównym bazę danych komendami:

  1. `docker exec -i  mysql-babd mysql -pp < schema.sql`
  
  2. `docker exec -i  mysql-babd mysql -pp < migration1.sql`
  
  3. `docker exec -i  mysql-babd mysql -pp < migration2.sql`
  
  4. `docker exec -i  mysql-babd mysql -pp < migration3.sql`

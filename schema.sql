SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `obrazy_medyczne` ;
CREATE SCHEMA IF NOT EXISTS `obrazy_medyczne` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `obrazy_medyczne` ;

-- -----------------------------------------------------
-- Table `obrazy_medyczne`.`lekarz`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `obrazy_medyczne`.`lekarz` ;

CREATE TABLE IF NOT EXISTS `obrazy_medyczne`.`lekarz` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `imie` VARCHAR(45) NOT NULL,
  `nazwisko` VARCHAR(45) NOT NULL,
  `specjalizacja` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `nazwisko` (`nazwisko` ASC, `imie` ASC),
  INDEX `specjalizacja` (`specjalizacja` ASC, `nazwisko` ASC, `imie` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `obrazy_medyczne`.`pacjent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `obrazy_medyczne`.`pacjent` ;

CREATE TABLE IF NOT EXISTS `obrazy_medyczne`.`pacjent` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `imie` VARCHAR(45) NOT NULL,
  `drugie_imie` VARCHAR(45) NULL,
  `nazwisko` VARCHAR(45) NOT NULL,
  `pesel` VARCHAR(9) NOT NULL,
  `adres` VARCHAR(45) NOT NULL,
  `telefon` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `pesel_UNIQUE` (`pesel` ASC),
  INDEX `nazwisko` (`nazwisko` ASC, `imie` ASC, `drugie_imie` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `obrazy_medyczne`.`obraz`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `obrazy_medyczne`.`obraz` ;

CREATE TABLE IF NOT EXISTS `obrazy_medyczne`.`obraz` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nazwa` VARCHAR(45) NOT NULL,
  `nazwa_pliku` VARCHAR(100) NOT NULL,
  `data_utworzenia` DATETIME NOT NULL,
  `lekarz_zlecajacy` INT NOT NULL,
  `lekarz_wykonujacy` INT NOT NULL,
  `pacjent_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nazwa_pliku_UNIQUE` (`nazwa_pliku` ASC),
  INDEX `fk_OBRAZ_LEKARZ_idx` (`lekarz_zlecajacy` ASC),
  INDEX `fk_OBRAZ_LEKARZ1_idx` (`lekarz_wykonujacy` ASC),
  INDEX `fk_OBRAZ_PACJENT1_idx` (`pacjent_id` ASC),
  CONSTRAINT `fk_OBRAZ_LEKARZ`
    FOREIGN KEY (`lekarz_zlecajacy`)
    REFERENCES `obrazy_medyczne`.`lekarz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OBRAZ_LEKARZ1`
    FOREIGN KEY (`lekarz_wykonujacy`)
    REFERENCES `obrazy_medyczne`.`lekarz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OBRAZ_PACJENT1`
    FOREIGN KEY (`pacjent_id`)
    REFERENCES `obrazy_medyczne`.`pacjent` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `obrazy_medyczne`.`placowka`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `obrazy_medyczne`.`placowka` ;

CREATE TABLE IF NOT EXISTS `obrazy_medyczne`.`placowka` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nazwa` VARCHAR(45) NOT NULL,
  `adres` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `obrazy_medyczne`.`lekarz_placowka_mm`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `obrazy_medyczne`.`lekarz_placowka_mm` ;

CREATE TABLE IF NOT EXISTS `obrazy_medyczne`.`lekarz_placowka_mm` (
  `lekarz_id` INT NOT NULL,
  `placowka_id` INT NOT NULL,
  PRIMARY KEY (`lekarz_id`, `placowka_id`),
  INDEX `fk_LEKARZ_has_PLACOWKA_PLACOWKA1_idx` (`placowka_id` ASC),
  INDEX `fk_LEKARZ_has_PLACOWKA_LEKARZ1_idx` (`lekarz_id` ASC),
  CONSTRAINT `fk_LEKARZ_has_PLACOWKA_LEKARZ1`
    FOREIGN KEY (`lekarz_id`)
    REFERENCES `obrazy_medyczne`.`lekarz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_LEKARZ_has_PLACOWKA_PLACOWKA1`
    FOREIGN KEY (`placowka_id`)
    REFERENCES `obrazy_medyczne`.`placowka` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


DELIMITER $$
USE `obrazy_medyczne`$$
CREATE FUNCTION `validate_pesel` (pesel varchar(11)) RETURNS int(1)
BEGIN
	declare waga integer default 0;
	declare sumaKontr integer default 0;
	declare i integer default 0;

	if (char_length(pesel) != 11) then
		return 0;
	end if;

	WHILE i < 10 DO
		set i=i+1;

		if i in (1,5,9) then
			SET waga = 1;
		elseif i in (2,6,10) then
			SET waga = 3;
		elseif i in (3,7) then
			SET waga = 7;
		elseif i in (4,8) then
			SET waga = 9;
		end if;

		set sumaKontr = sumaKontr + (waga * substr(pesel,i,1));
	end WHILE;

	set sumaKontr = (10 - (sumaKontr % 10)) % 10;

	if (sumaKontr = substr(pesel,11,1)) then
		return 1;
	else
		return 0;
	end if;
END $$

DELIMITER ;

DELIMITER $$

USE `obrazy_medyczne`$$
CREATE TRIGGER `pacjent_BINS` BEFORE INSERT ON `pacjent` FOR EACH ROW
BEGIN
	IF validate_pesel(NEW.`pesel`) = 0 THEN
		signal sqlstate '45000';
	END if;
END$$

USE `obrazy_medyczne`$$
CREATE TRIGGER `pacjent_BUPD` BEFORE UPDATE ON `pacjent` FOR EACH ROW
BEGIN
    IF validate_pesel(NEW.`pesel`) = 0 THEN
		signal sqlstate '45000';
    END if;
END
$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

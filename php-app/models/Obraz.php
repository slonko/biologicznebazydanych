<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obraz".
 *
 * @property integer $id
 * @property string $nazwa
 * @property string $nazwa_pliku
 * @property string $data_utworzenia
 * @property integer $lekarz_zlecajacy
 * @property integer $lekarz_wykonujacy
 * @property integer $pacjent_id
 *
 * @property Lekarz $lekarzZlecajacy
 * @property Lekarz $lekarzWykonujacy
 * @property Pacjent $pacjent
 */
class Obraz extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obraz';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazwa', 'nazwa_pliku', 'data_utworzenia', 'lekarz_zlecajacy', 'lekarz_wykonujacy', 'pacjent_id'], 'required'],
            [['data_utworzenia'], 'safe'],
            [['data_utworzenia'], 'date', 'format' => 'yyyy-MM-dd'],
            [['lekarz_zlecajacy', 'lekarz_wykonujacy', 'pacjent_id'], 'integer'],
            [['nazwa'], 'string', 'max' => 45],
            [['nazwa_pliku'], 'string', 'max' => 100],
            [['nazwa_pliku'], 'unique'],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nazwa' => 'Nazwa',
            'nazwa_pliku' => 'Obraz',
            'data_utworzenia' => 'Data Utworzenia',
            'lekarz_zlecajacy' => 'Lekarz Zlecający',
            'lekarz_wykonujacy' => 'Lekarz Wykonujący',
            'pacjent_id' => 'Pacjent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLekarzZlecajacy()
    {
        return $this->hasOne(Lekarz::className(), ['id' => 'lekarz_zlecajacy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLekarzWykonujacy()
    {
        return $this->hasOne(Lekarz::className(), ['id' => 'lekarz_wykonujacy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacjent()
    {
        return $this->hasOne(Pacjent::className(), ['id' => 'pacjent_id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lekarz".
 *
 * @property integer $id
 * @property string $imie
 * @property string $nazwisko
 * @property string $specjalizacja
 *
 * @property Placowka[] $placowkas
 * @property Obraz[] $obrazs
 */
class Lekarz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lekarz';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imie', 'nazwisko', 'specjalizacja'], 'required'],
            [['imie', 'nazwisko', 'specjalizacja'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imie' => 'Imię',
            'nazwisko' => 'Nazwisko',
            'specjalizacja' => 'Specjalizacja',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacowkas()
    {
        return $this->hasMany(Placowka::className(), ['id' => 'placowka_id'])->viaTable('lekarz_placowka_mm', ['lekarz_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObrazs()
    {
        return $this->hasMany(Obraz::className(), ['lekarz_wykonujacy' => 'id']);
    }

    /**
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwisko . ' ' . $this->imie;
    }
}

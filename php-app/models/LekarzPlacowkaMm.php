<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lekarz_placowka_mm".
 *
 * @property integer $lekarz_id
 * @property integer $placowka_id
 *
 * @property Lekarz $lekarz
 * @property Placowka $placowka
 */
class LekarzPlacowkaMm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lekarz_placowka_mm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lekarz_id', 'placowka_id'], 'required'],
            [['lekarz_id', 'placowka_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lekarz_id' => 'Lekarz ID',
            'placowka_id' => 'Placowka ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLekarz()
    {
        return $this->hasOne(Lekarz::className(), ['id' => 'lekarz_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacowka()
    {
        return $this->hasOne(Placowka::className(), ['id' => 'placowka_id']);
    }
}

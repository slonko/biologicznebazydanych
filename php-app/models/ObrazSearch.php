<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Obraz;

/**
 * ObrazSearch represents the model behind the search form about `app\models\Obraz`.
 */
class ObrazSearch extends Obraz
{
    /**
     * @var string
     */
    public $data_od;

    /**
     * @var string
     */
    public $data_do;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lekarz_zlecajacy', 'lekarz_wykonujacy', 'pacjent_id'], 'integer'],
            [['nazwa', 'nazwa_pliku', 'data_od', 'data_do'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Obraz::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lekarz_zlecajacy' => $this->lekarz_zlecajacy,
            'lekarz_wykonujacy' => $this->lekarz_wykonujacy,
            'pacjent_id' => $this->pacjent_id,
        ]);

        $query->andFilterWhere(['like', 'nazwa', $this->nazwa])
            ->andFilterWhere(['like', 'nazwa_pliku', $this->nazwa_pliku]);
        if($this->data_od){
            $query->andFilterWhere(['>=', 'data_utworzenia', $this->data_od . '00:00:00']);
        }
        if($this->data_do){
            $query->andFilterWhere(['<=', 'data_utworzenia', $this->data_do . '23:59:59']);
        }

        return $dataProvider;
    }
}

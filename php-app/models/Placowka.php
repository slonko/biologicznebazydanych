<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "placowka".
 *
 * @property integer $id
 * @property string $nazwa
 * @property string $adres
 *
 * @property Lekarz[] $lekarzs
 */
class Placowka extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'placowka';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nazwa', 'adres'], 'required'],
            [['nazwa'], 'string', 'max' => 45],
            [['adres'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nazwa' => 'Nazwa',
            'adres' => 'Adres',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLekarzs()
    {
        return $this->hasMany(Lekarz::className(), ['id' => 'lekarz_id'])->viaTable('lekarz_placowka_mm', ['placowka_id' => 'id']);
    }
}

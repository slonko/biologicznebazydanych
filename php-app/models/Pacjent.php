<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pacjent".
 *
 * @property integer $id
 * @property string $imie
 * @property string $drugie_imie
 * @property string $nazwisko
 * @property string $pesel
 * @property string $adres
 * @property string $telefon
 *
 * @property Obraz[] $obrazs
 */
class Pacjent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pacjent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imie', 'nazwisko', 'pesel', 'adres'], 'required'],
            [['imie', 'drugie_imie', 'nazwisko'], 'string', 'max' => 45],
            [['adres'], 'string', 'max' => 255],
            [['telefon'], 'string', 'max' => 15],
            [['pesel'], 'string', 'length' => 11],
            [['pesel'], 'unique'],
            [['pesel'], function ($attribute, $params) {
                $pesel = $this->$attribute;
                $validPesel = 1 * $pesel[0] + 3 * $pesel[1] + 7 * $pesel[2] + 9 * $pesel[3] + 1 * $pesel[4]
                    + 3 * $pesel[5] + 7 * $pesel[6] + 9 * $pesel[7] + 1 * $pesel[8] + 3 * $pesel[9];
                if ((10 - ($validPesel % 10)) % 10 != $pesel[10]) {
                    $this->addError($attribute, 'PESEL jest niepoprawny.');
                }
            }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imie' => 'Imie',
            'drugie_imie' => 'Drugie Imie',
            'nazwisko' => 'Nazwisko',
            'pesel' => 'Pesel',
            'adres' => 'Adres',
            'telefon' => 'Telefon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObrazs()
    {
        return $this->hasMany(Obraz::className(), ['pacjent_id' => 'id']);
    }

    public function getNazwa()
    {
        return $this->nazwisko . ' ' . $this->imie;
    }
}

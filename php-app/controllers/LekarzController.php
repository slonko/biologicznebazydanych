<?php

namespace app\controllers;

use app\models\LekarzPlacowkaMm;
use app\models\Placowka;
use Yii;
use app\models\Lekarz;
use app\models\LekarzSearch;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LekarzController implements the CRUD actions for Lekarz model.
 */
class LekarzController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lekarz models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LekarzSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lekarz model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'availablePlacowkas' => Placowka::find()
                ->where(['not in', 'id', (new Query())->select('placowka_id')
                    ->from(LekarzPlacowkaMm::tableName())->where(['lekarz_id' => $id])])
                ->all(),
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Lekarz model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Lekarz();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Dodanie lekarza powiodło się.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Lekarz model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->getSession()->setFlash('success', 'Edycja lekarza powiodła się.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Lekarz model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try{
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', 'Usunięcie lekarza powiodło się.');
        }catch (\Exception $e){
            Yii::$app->getSession()->setFlash('danger', 'Usunięcie lekarza nie powiodło się. Usuń wszystkie obraz, które lekarz zlecał lub wykonywał, aby usunąć tego lekarza.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Placowka belonged to Lekarz .
     * If deletion is successful, the browser will be redirected to the Lekarz 'view' page.
     * @param integer $idPlacowka
     * @param integer $idLekarz
     * @return mixed
     */
    public function actionDeletePlacowka($idPlacowka, $idLekarz)
    {
        /** @var LekarzPlacowkaMm $model */
        $model = LekarzPlacowkaMm::findOne(['placowka_id' => $idPlacowka, 'lekarz_id' => $idLekarz]);
        if (!$model) {
            Yii::$app->getSession()->setFlash('danger', 'Usunięcie placowki, do ktorej należał lekarz nie powiodło się.');
        }
        $model->delete();
        Yii::$app->getSession()->setFlash('success', 'Usunięcie placowki, do ktorej należał lekarz powiodło się.');
        return $this->redirect(['view', 'id' => $idLekarz]);
    }

    /**
     * Add an existing Placowka to Lekarz.
     * If adding is successful, the browser will be redirected to the Lekarz 'view' page.
     * @return mixed
     */
    public function actionCreatePlacowka()
    {
        $model = new LekarzPlacowkaMm();

        $model->placowka_id = Yii::$app->request->post()['placowka_id'];
        $model->lekarz_id = Yii::$app->request->post()['lekarz_id'];

        if (!$model->save()) {
            Yii::$app->getSession()->setFlash('danger', 'Dodanie placowki, do ktorej należy lekarz nie powiodło się.');
        }

        Yii::$app->getSession()->setFlash('success', 'Dodanie placowki, do ktorej należy lekarz powiodło się.');
        return $this->redirect(['view', 'id' => $model->placowka_id]);
    }

    /**
     * Finds the Lekarz model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lekarz the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lekarz::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

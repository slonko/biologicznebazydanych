<?php

namespace app\controllers;

use Yii;
use app\models\Obraz;
use app\models\ObrazSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ObrazController implements the CRUD actions for Obraz model.
 */
class ObrazController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Obraz models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ObrazSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Obraz model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Obraz model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Obraz();
        try {
            if (!$model->load(Yii::$app->request->post())) {
                throw new \Exception;
            }
            if (!$model->file = UploadedFile::getInstance($model, 'file')) {
                $model->addError('file', 'Wystąpił błąd z wgrywanym plikiem');
                throw new \Exception;
            }

            $fileName = $model->file->baseName . '.' . $model->file->extension;

            if (is_file('uploads/' . $fileName)) {
                $model->addError('file', 'Plik o tej nazwie już istnieje');
                throw new \Exception;
            }
            if (!$model->file->saveAs('uploads/' . $fileName)) {
                $model->addError('file', 'Nie można zapisać pliku');
                throw new \Exception;
            }

            // saving the path in the db column
            $model->nazwa_pliku = $fileName;
            if (!$model->save()) {
                if (!unlink('uploads/' . $model->nazwa_pliku)) {
                    throw new \Exception;
                }
                Yii::$app->getSession()->setFlash('danger', 'Dodanie obrazu nie powiodło się.');
                throw new \Exception;
            }

            Yii::$app->getSession()->setFlash('success', 'Dodanie obrazu powiodło się');
            return $this->redirect(['view', 'id' => $model->id]);
        } catch (\Exception $e) {
            foreach ($model->getErrors('nazwa_pliku') as $error) {
                $model->addError('file', $error);
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Obraz model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        try {
            if (!$model->load(Yii::$app->request->post())) {
                throw new \Exception;
            }

            if ($model->file = UploadedFile::getInstance($model, 'file')) {
                $fileName = $model->file->baseName . '.' . $model->file->extension;
                if (is_file('uploads/' . $fileName)) {
                    $model->addError('file', 'Plik o tej nazwie już istnieje');
                    throw new \Exception;
                }
                if (!$model->file->saveAs('uploads/' . $fileName)) {
                    $model->addError('file', 'Nie można zapisać pliku');
                    throw new \Exception;
                }
                if($model->nazwa_pliku){
                    unlink('uploads/' . $model->nazwa_pliku);
                }
                $model->nazwa_pliku = $fileName;
            }
            if (!$model->save()) {
                throw new \Exception;
            }
            Yii::$app->getSession()->setFlash('success', 'Edycja obrazu powiodła się');
            return $this->redirect(['view', 'id' => $model->id]);
        } catch (\Exception $e) {
            foreach ($model->getErrors('nazwa_pliku') as $error) {
                $model->addError('file', $error);
            }
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->getSession()->setFlash('danger', 'Edycja obrazu nie powiodła się.');
            }
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Obraz model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        try{
            if (unlink('uploads/' . $model->nazwa_pliku)) {
                $model->delete();
            }
            Yii::$app->getSession()->setFlash('success', 'Usunięcie obrazu powiodło się.');
        }catch (\Exception $e){
            Yii::$app->getSession()->setFlash('danger', 'Usunięcie obrazu nie powiodło się.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Obraz model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Obraz the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Obraz::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use app\models\Lekarz;
use app\models\LekarzPlacowkaMm;
use app\models\Pacjent;
use Yii;
use app\models\Placowka;
use app\models\PlacowkaSearch;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PlacowkaController implements the CRUD actions for Placowka model.
 */
class PlacowkaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'create-lekarz' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Placowka models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlacowkaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Placowka model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'availableLekarzs' => Lekarz::find()
                ->where(['not in', 'id', (new Query())->select('lekarz_id')
                    ->from(LekarzPlacowkaMm::tableName())->where(['placowka_id' => $id])])
                ->all(),
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Placowka model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Placowka();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Dodanie placówki powiodło się');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Placowka model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Edycja placówki powiodła się');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Placowka model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', 'Usuwanie powiodło się.');
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('danger', 'Usuwanie nie powiodło się. Usuń wszystkich lekarzy zatrudnionych, aby usunąć placówkę.');
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Lakarz form Placowka.
     * If deletion is successful, the browser will be redirected to the Placowka 'view' page.
     * @param integer $idPlacowka
     * @param integer $idLekarz
     * @return mixed
     */
    public function actionDeleteLekarz($idPlacowka, $idLekarz)
    {
        /** @var LekarzPlacowkaMm $model */
        $model = LekarzPlacowkaMm::findOne(['placowka_id' => $idPlacowka, 'lekarz_id' => $idLekarz]);
        if (!$model) {
            Yii::$app->getSession()->setFlash('danger', 'Usunięcie lekarza z placówki nie powiodło się.');
        }
        $model->delete();
        Yii::$app->getSession()->setFlash('success', 'Usunięcie lekarza z placówki powiodło się.');
        return $this->redirect(['view', 'id' => $idPlacowka]);
    }

    /**
     * Add an existing Lakarz to Placowka.
     * If adding is successful, the browser will be redirected to the Placowka 'view' page.
     * @return mixed
     */
    public function actionCreateLekarz()
    {
        $model = new LekarzPlacowkaMm();

        $model->placowka_id = Yii::$app->request->post()['placowka_id'];
        $model->lekarz_id = Yii::$app->request->post()['lekarz_id'];

        if (!$model->save()) {
            Yii::$app->getSession()->setFlash('danger', 'Dodanie lekarza do placówki nie powiodło się.');
        }

        Yii::$app->getSession()->setFlash('success', 'Dodanie lekarza do placówki powiodło się.');
        return $this->redirect(['view', 'id' => $model->placowka_id]);
    }

    /**
     * Finds the Placowka model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Placowka the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Placowka::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

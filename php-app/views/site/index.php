<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron content">
        <h1>Baza Obrazów Medycznych</h1>

        <p><?= Html::a('Przeglądaj obrazy',['obraz/index'],['class'=>'btn btn-lg btn-success'])?></p>
    </div>
</div>

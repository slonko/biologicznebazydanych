<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lekarz */
/* @var $availablePlacowkas Placowka[] */

$this->title = $model->nazwisko . ' ' . $model->imie;
$this->params['breadcrumbs'][] = ['label' => 'Lekarze', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lekarz-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Edytuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'imie',
            'nazwisko',
            'specjalizacja',
        ],
    ]) ?>

    <h3>Zatrudniony w placówce</h3>
    <table class="table table-striped table-bordered detail-view">
        <?php
        foreach ($model->placowkas as $placowka) {
            echo '<tr><td>' . Html::a($placowka->nazwa, ['placowka/view', 'id' => $placowka->id], ['class' => 'lbtn'])
                . Html::a('Usuń placówkę', ['delete-placowka', 'idLekarz' => $model->id, 'idPlacowka' => $placowka->id],
                    ['class' => 'btn btn-danger btn-sm', 'style' => 'float:right'])
                .  '</td></tr>';
        }
        ?>
    </table>


    <?= Html::a('wszystkie zlecenia', ['obraz/index', 'ObrazSearch[lekarz_zlecajacy]'=>$model->id], ['class' => 'btn btn-info']) ?>
    <?= Html::a('wszystkie wykonania', ['obraz/index', 'ObrazSearch[lekarz_wykonujacy]'=>$model->id], ['class' => 'btn btn-info']) ?>

    <h3>Dodaj placówkę</h3>

    <?= Html::beginForm(['create-placowka']) ?>
    <?= Html::hiddenInput('placowka_id', $model->id) ?>
    <?= Html::dropDownList('lekarz_id', null,
        ArrayHelper::map($availablePlacowkas, 'id', 'nazwa'),
        ['prompt' => '','required' => '' , 'class' => 'btn btn-default dropdown-toggle']) ?>
    <?= Html::submitButton('Dodaj placowkę',
        ['class' => 'btn btn-success']) ?>
    <?= Html::endForm() ?>
</div>

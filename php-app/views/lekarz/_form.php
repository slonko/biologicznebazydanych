<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lekarz */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lekarz-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'imie')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'nazwisko')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'specjalizacja')->textInput(['maxlength' => 45]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Zatwierdź' : 'Zatwierdź', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lekarz */

$this->title = 'Edytuj Lekarza: ' . ' ' . $model->nazwisko . ' ' . $model->imie;
$this->params['breadcrumbs'][] = ['label' => 'Lekarzs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edytuj';
?>
<div class="lekarz-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

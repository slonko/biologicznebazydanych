<?php

use app\models\Lekarz;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Placowka */
/* @var $availableLekarzs Lekarz[] */

$this->title = $model->nazwa;
$this->params['breadcrumbs'][] = ['label' => 'Placówki', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placowka-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Edytuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Czy na pewno chcesz usunąć tę placówkę?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nazwa',
            'adres',
        ],
    ]) ?>

    <h3>Zatrudnieni lekarze</h3>

    <table class="table table-striped table-bordered detail-view">
        <?php
        foreach ($model->lekarzs as $lekarz) {
            echo '<tr><td>' . Html::a($lekarz->getNazwa(), ['lekarz/view', 'id' => $lekarz->id], ['class' => 'lbtn'])
                . Html::a('Usuń z placówki', ['delete-lekarz', 'idPlacowka' => $model->id, 'idLekarz' => $lekarz->id],
                    ['class' => 'btn btn-danger btn-sm', 'style' => 'float:right'])
                . '</td></tr>';
        } ?>
    </table>

    <h3>Dodaj lekarza</h3>

    <?= Html::beginForm(['create-lekarz']) ?>
        <?= Html::hiddenInput('placowka_id', $model->id) ?>
        <?= Html::dropDownList('lekarz_id', null,
            ArrayHelper::map($availableLekarzs, 'id', 'nazwa'),
            ['prompt' => '','required' => '' , 'class' => 'btn btn-default dropdown-toggle']) ?>
        <?= Html::submitButton('Dodaj lekarza',
            ['class' => 'btn btn-success']) ?>
    <?= Html::endForm() ?>
</div>

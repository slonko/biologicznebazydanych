<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Placowka */

$this->title = 'Dodaj Placowkę';
$this->params['breadcrumbs'][] = ['label' => 'Placowki', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placowka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

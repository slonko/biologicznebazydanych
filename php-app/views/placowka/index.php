<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlacowkaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Placówki';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placowka-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Dodaj Placowkę', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nazwa',
            'adres',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Placowka */

$this->title = 'Edytuj Placowka: ' . ' ' . $model->nazwa;
$this->params['breadcrumbs'][] = ['label' => 'Placowki', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nazwa, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edytuj';
?>
<div class="placowka-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

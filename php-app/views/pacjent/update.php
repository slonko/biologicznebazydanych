<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pacjent */

$this->title = 'Edytuj Pacjenta: ' . ' ' . $model->nazwisko . ' ' . $model->imie;
$this->params['breadcrumbs'][] = ['label' => 'Pacjenci', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nazwisko, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edytuj';
?>
<div class="pacjent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

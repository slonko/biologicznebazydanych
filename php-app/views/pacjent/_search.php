<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PacjentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pacjent-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'imie') ?>

    <?= $form->field($model, 'drugie_imie') ?>

    <?= $form->field($model, 'nazwisko') ?>

    <?= $form->field($model, 'pesel') ?>

    <?php // echo $form->field($model, 'adres') ?>

    <?php // echo $form->field($model, 'telefon') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pacjent */

$this->title = 'Dodaj Pacjenta';
$this->params['breadcrumbs'][] = ['label' => 'Pacjenci', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pacjent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

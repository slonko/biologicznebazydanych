<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PacjentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pacjenci';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pacjent-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Dodaj Pacjenta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'imie',
            'drugie_imie',
            'nazwisko',
            'pesel',
            // 'adres',
            // 'telefon',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

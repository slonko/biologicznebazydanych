<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pacjent */

$this->title = $model->nazwisko . ' ' . $model->imie;
$this->params['breadcrumbs'][] = ['label' => 'Pacjenci', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pacjent-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Edytuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Czy na pewno chcesz usunąć tego pacjenta?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'imie',
            'drugie_imie',
            'nazwisko',
            'pesel',
            'adres',
            'telefon',
        ],
    ]) ?>

    <?=Html::a('wszystkie obrazy', ['obraz/index', 'ObrazSearch[pacjent_id]'=> $model->id], ['class' => 'btn btn-info']) ?>
</div>

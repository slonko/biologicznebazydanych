<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pacjent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pacjent-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'imie')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'drugie_imie')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'nazwisko')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'pesel')->textInput(['maxlength' => 11]) ?>

    <?= $form->field($model, 'adres')->textarea(['maxlength' => 255]) ?>

    <?= $form->field($model, 'telefon')->textInput(['maxlength' => 15]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Zatwierdź' : 'Zatwierdź', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Obraz */

$this->title = 'Edytuj Obraz: ' . ' ' . $model->nazwa;
$this->params['breadcrumbs'][] = ['label' => 'Obrazy', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nazwa, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edytuj';
?>
<div class="obraz-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

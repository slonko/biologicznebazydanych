<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Obraz */

$this->title = $model->nazwa;
$this->params['breadcrumbs'][] = ['label' => 'Obrazy', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obraz-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Edytuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Czy na pewno chcesz usunąć ten obraz?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nazwa',
            [
                'attribute' => 'nazwa_pliku',
                'format' => 'html',
                'value' => Html::img('uploads/' . $model->nazwa_pliku),
            ],
            'data_utworzenia',
            [
                'attribute' => 'lekarz_zlecajacy',
                'format' => 'html',
                'value' => Html::a($model->lekarzZlecajacy->getNazwa(), ['lekarz/view', 'id' => $model->lekarzZlecajacy->id]),
            ],
            [
                'attribute' => 'lekarz_wykonujacy',
                'format' => 'html',
                'value' => Html::a($model->lekarzWykonujacy->getNazwa(), ['lekarz/view', 'id' => $model->lekarzWykonujacy->id]),
            ],
            [
                'attribute' => 'pacjent_id',
                'format' => 'html',
                'value' => Html::a($model->pacjent->getNazwa(), ['pacjent/view', 'id' => $model->pacjent->id]),
            ],
        ],
    ]) ?>

</div>

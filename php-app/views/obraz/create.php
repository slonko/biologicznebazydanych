<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Obraz */

$this->title = 'Dodaj Obraz';
$this->params['breadcrumbs'][] = ['label' => 'Obrazy', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obraz-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

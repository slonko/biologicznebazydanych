<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObrazSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obraz-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nazwa') ?>

    <?= $form->field($model, 'nazwa_pliku') ?>

    <?= $form->field($model, 'data_utworzenia') ?>

    <?= $form->field($model, 'lekarz_zlecajacy') ?>

    <?php // echo $form->field($model, 'lekarz_wykonujacy') ?>

    <?php // echo $form->field($model, 'pacjent_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

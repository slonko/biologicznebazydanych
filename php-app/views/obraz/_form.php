<?php

use app\models\Lekarz;
use app\models\Pacjent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Obraz */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obraz-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'nazwa')->textInput(['maxlength' => 45]) ?>

    <?php if ($model->nazwa_pliku) {
        echo Html::img('uploads/' . $model->nazwa_pliku);
    } ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <?= $form->field($model, 'data_utworzenia')->textInput() ?>

    <?= $form->field($model, 'lekarz_zlecajacy')
        ->dropDownList(
            ArrayHelper::map(Lekarz::find()->all(), 'id', 'nazwa'),
            ['prompt' => '']
        ); ?>

    <?= $form->field($model, 'lekarz_wykonujacy')
        ->dropDownList(
            ArrayHelper::map(Lekarz::find()->all(), 'id', 'nazwa'),
            ['prompt' => '']
        ); ?>

    <?= $form->field($model, 'pacjent_id')
        ->dropDownList(
            ArrayHelper::map(Pacjent::find()->all(), 'id', 'nazwa'),
            ['prompt' => '']
        ); ?>

    <div class="form-group">
        <?= Html::submitButton('Zatwierdź', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

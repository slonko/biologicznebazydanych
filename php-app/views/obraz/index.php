<?php

use app\models\Lekarz;
use app\models\Obraz;
use app\models\Pacjent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ObrazSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obrazy';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obraz-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Dodaj Obraz', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '70'],
            ],
            'nazwa',
            [
                'attribute' => 'nazwa_pliku',
                'format' => 'html',
                'value' => function (Obraz $model) {
                    return Html::img('uploads/' . $model->nazwa_pliku);
                },
            ],
            [
                'attribute' => 'data_utworzenia',
                'filter' =>
                    Html::tag('span', 'od:') . Html::activeInput('date', $searchModel, 'data_od', ['class' => 'form-control']) . Html::tag('br')
                    . Html::tag('span', 'do:') . Html::activeInput('date', $searchModel, 'data_do', ['class' => 'form-control']),
            ],
            [
                'attribute' => 'lekarz_zlecajacy',
                'format' => 'html',
                'value' => function (Obraz $model) {
                    return Html::a($model->lekarzZlecajacy->getNazwa(), ['lekarz/view', 'id' => $model->lekarzZlecajacy->id]);
                },
                'filter' => ArrayHelper::map(Lekarz::find()->all(), 'id', 'nazwa'),
            ],
            [
                'attribute' => 'lekarz_wykonujacy',
                'format' => 'html',
                'value' => function (Obraz $model) {
                    return Html::a($model->lekarzWykonujacy->getNazwa(), ['lekarz/view', 'id' => $model->lekarzWykonujacy->id]);
                },
                'filter' => ArrayHelper::map(Lekarz::find()->all(), 'id', 'nazwa'),
            ],
            [
                'attribute' => 'pacjent_id',
                'format' => 'html',
                'value' => function (Obraz $model) {
                    return Html::a($model->pacjent->getNazwa(), ['pacjent/view', 'id' => $model->pacjent->id]);
                },
                'filter' => ArrayHelper::map(Pacjent::find()->all(), 'id', 'nazwa'),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'text-nowrap'],
            ],
        ],
    ]); ?>

</div>
